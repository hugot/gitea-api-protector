package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// VerifyUserHandler implements http.Handler
type VerifyUserHandler struct {
	db     UserIDStore
	secret string
}

type PullRequestData struct {
	PullRequest struct {
		User struct {
			ID uint32 `json:"id"`
		} `json:"user"`
		Merged bool `json:"merged"`
	} `json:"pull_request"`
	Action string `json:"action"`
	Secret string `json:"secret"`
}

func (h *VerifyUserHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	pr := &PullRequestData{}

	err := json.NewDecoder(bufio.NewReader(r.Body)).Decode(pr)

	if err != nil {
		log.Println(err)
		InternalServerError(w)

		return
	}

	if pr.Secret != h.secret {
		log.Println("Got unauthorized request to /verify endpoint")
		w.WriteHeader(401)
		w.Write([]byte("401 Unauthorized"))

		return
	}

	log.Println(
		fmt.Sprintf(
			"Received verification request for %s pull request, user id: %d",
			pr.Action,
			pr.PullRequest.User.ID,
		),
	)

	if pr.Action == "closed" && pr.PullRequest.Merged {
		log.Println(fmt.Sprintf("Adding user with id %d to verified users", pr.PullRequest.User.ID))
		err := h.db.AddUser(pr.PullRequest.User.ID)

		if err != nil {
			log.Println(err)
			InternalServerError(w)

			return
		}

		w.WriteHeader(http.StatusCreated)
		w.Write([]byte("Verified user added"))

		return
	}

	w.Write([]byte("Hook received"))
}
