package main

import "net/http"

func InternalServerError(w http.ResponseWriter) {
	w.WriteHeader(500)
	w.Write([]byte("500 Internal server error"))
}

func Authorized(w http.ResponseWriter) {
	w.WriteHeader(200)
	w.Write([]byte("Authorized!"))
}
