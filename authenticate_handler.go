package main

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

const passthroughHeaderName string = "API-Protector-Passthrough"

var (
	ErrUnauthorizedUser  error = errors.New("User not authorized, invalid token")
	ErrUserInfoRetrieval error = errors.New("Failed to retrieve user information from gitea")
)

type UserDataRequester func(token string, passthroughToken string) (*UserData, error)

type AuthenticateHandler struct {
	db              UserIDStore
	keyCache        *KeyCache
	ownKeyCache     *KeyCache
	requestUserData UserDataRequester
}

func NewAuthenticateHandler(
	giteaURL string,
	db UserIDStore,
	cachedKeyExpiration time.Duration,
) *AuthenticateHandler {
	return &AuthenticateHandler{
		db:              db,
		keyCache:        NewKeyCache(300*time.Second, cachedKeyExpiration),
		requestUserData: MakeUserDataRequester(giteaURL),
		ownKeyCache:     NewKeyCache(120*time.Second, 200*time.Second),
	}
}

func (h *AuthenticateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// A passthrough header means that this is a request from gitea-api-authenticator
	// itself. Access should be granted.
	passthroughHeader := r.Header.Get(passthroughHeaderName)
	if passthroughHeader != "" {
		passthroughToken := strings.Replace(passthroughHeader, "bearer ", "", 1)

		if h.ownKeyCache.HasKey(passthroughToken) {
			Authorized(w)

			return
		}

		w.WriteHeader(http.StatusUnauthorized)

		return
	}

	token := strings.Replace(r.Header.Get("Authorization"), "bearer ", "", 1)
	query := r.URL.Query()

	if token == "" {
		token = query.Get("access_token")
	}

	if token == "" {
		token = query.Get("token")
	}

	if token == "" {
		log.Println("Received auth request without token.")
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("You must be a verified user to have API access"))

		return
	}

	if h.keyCache.HasKey(token) {
		Authorized(w)

		return
	}

	//  Generate a recognizable passthrough token to allow own request to pass through and
	//  retrieve user data.
	passthroughToken, err := generateRandomKey()
	if err != nil {
		log.Println(err)
		InternalServerError(w)

		return
	}

	h.ownKeyCache.AddKey(passthroughToken)

	userData, err := h.requestUserData(token, passthroughToken)
	if err != nil {
		if err == ErrUnauthorizedUser {
			// Empty 401 just like gitea would return for an invalid token
			w.WriteHeader(http.StatusUnauthorized)

			return
		}

		log.Println(err)
		InternalServerError(w)

		return
	}

	if h.db.HasUser(userData.ID) || userData.IsAdmin {
		log.Println(
			fmt.Sprintf("User with ID %d is verified, caching token", userData.ID),
		)
		h.keyCache.AddKey(token)

		Authorized(w)

		return
	}

	w.WriteHeader(http.StatusUnauthorized)
	w.Write([]byte("You must be a verified user to have API access"))
}

type UserData struct {
	ID      uint32 `json:"id"`
	IsAdmin bool   `json:"is_admin"`
}

// Creates a closure that requests a gitea user's data via the HTTP API
func MakeUserDataRequester(giteaURL string) UserDataRequester {
	APIEndPoint := giteaURL + "/api/v1/user"

	return func(token string, passthroughToken string) (*UserData, error) {
		req, err := http.NewRequest(
			"GET",
			APIEndPoint,
			nil,
		)

		if err != nil {
			return nil, err
		}

		req.Header.Set("Authorization", "bearer "+token)
		req.Header.Set(passthroughHeaderName, "bearer "+passthroughToken)

		res, err := http.DefaultClient.Do(req)

		if err != nil {
			return nil, err
		}

		if res.StatusCode != http.StatusOK {
			if res.StatusCode == http.StatusUnauthorized {
				return nil, ErrUnauthorizedUser
			}

			return nil, ErrUserInfoRetrieval
		}

		userData := &UserData{}

		err = json.NewDecoder(res.Body).Decode(userData)

		return userData, err
	}
}

// Generates a secure, base64 encoded random string using crypto/rand
func generateRandomKey() (string, error) {
	data := make([]byte, 31)
	_, err := rand.Read(data)

	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(data), nil
}
