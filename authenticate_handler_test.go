package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func MakeFakeDataRequester(
	userData *UserData,
	err error,
	checker func(token string, passthroughToken string),
) UserDataRequester {
	return func(token string, passthroughToken string) (*UserData, error) {
		checker(token, passthroughToken)
		return userData, err
	}
}

func MakePassthroughChecker(
	assertToken string,
	handler **AuthenticateHandler,
	t *testing.T,
) func(token string, passthroughToken string) {
	return func(token string, passthroughToken string) {
		if token != assertToken {
			t.Error(
				fmt.Sprintf(
					"Wrong user token was passed to execute passthrough request."+
						"\tExpected: %s\n\tGot: %s",
					assertToken,
					token,
				),
			)
		}

		response := httptest.NewRecorder()
		request := httptest.NewRequest("POST", "/auth", nil)
		request.Header.Set(passthroughHeaderName, "bearer "+passthroughToken)

		(*handler).ServeHTTP(response, request)

		status := response.Result().StatusCode
		if status != http.StatusOK {
			respBody, _ := ioutil.ReadAll(response.Result().Body)
			t.Error(
				fmt.Sprintf(
					"Passthrough request was not allowed when it should: %d - %s -- %s",
					status,
					http.StatusText(status),
					respBody,
				),
			)
		}
	}
}

func MakePassthroughCheckingDataRequester(
	handler **AuthenticateHandler,
	userData *UserData,
	assertToken string,
	t *testing.T,
) UserDataRequester {
	return MakeFakeDataRequester(
		userData,
		nil,
		MakePassthroughChecker(assertToken, handler, t),
	)
}

func AssertSuccessfullAuthenticationWIthRequest(
	request *http.Request,
	token string,
	t *testing.T,
) {
	response := httptest.NewRecorder()

	var handler *AuthenticateHandler

	userData := &UserData{
		ID:      5,
		IsAdmin: false,
	}

	db := NewInMemoryUSerIDStore()
	db.AddUser(5)

	dataRequester := MakePassthroughCheckingDataRequester(&handler, userData, token, t)
	handler = &AuthenticateHandler{
		db:              db,
		keyCache:        NewKeyCache(300*time.Second, 610*time.Second),
		requestUserData: dataRequester,
		ownKeyCache:     NewKeyCache(120*time.Second, 200*time.Second),
	}

	handler.ServeHTTP(response, request)

	status := response.Result().StatusCode
	if status != http.StatusOK {
		respBody, _ := ioutil.ReadAll(response.Result().Body)
		t.Error(
			fmt.Sprintf(
				"API Access was not granted to a verified user: %d - %s -- %s",
				status,
				http.StatusText(status),
				respBody,
			),
		)
	}
}

func TestAuthenticationFromHeaderNotCached(t *testing.T) {
	request := httptest.NewRequest("POST", "/auth", nil)
	token := "super-secret-token-string"
	request.Header.Set("Authorization", "bearer "+token)

	AssertSuccessfullAuthenticationWIthRequest(request, token, t)
}

func TestAuthenticationFromTokenParamNotCached(t *testing.T) {
	request := httptest.NewRequest("POST", "/auth", nil)
	token := "super-secret-token-string"

	query := request.URL.Query()
	query.Add("token", token)

	request.URL.RawQuery = query.Encode()

	AssertSuccessfullAuthenticationWIthRequest(request, token, t)
}

func TestAuthenticationFromAccessTokenParamNotCached(t *testing.T) {
	request := httptest.NewRequest("POST", "/auth", nil)
	token := "super-secret-token-string"

	query := request.URL.Query()
	query.Add("access_token", token)

	request.URL.RawQuery = query.Encode()

	AssertSuccessfullAuthenticationWIthRequest(request, token, t)
}

func TestUnauthorizedInvalidToken(t *testing.T) {
	token := "super-secret-token-string"
	request := httptest.NewRequest("POST", "/auth", nil)
	response := httptest.NewRecorder()

	request.Header.Set("Authorization", "bearer "+token)

	var handler *AuthenticateHandler

	dataRequester := MakeFakeDataRequester(
		nil,
		ErrUnauthorizedUser,
		MakePassthroughChecker(token, &handler, t),
	)

	handler = &AuthenticateHandler{
		db:              NewInMemoryUSerIDStore(),
		keyCache:        NewKeyCache(300*time.Second, 610*time.Second),
		requestUserData: dataRequester,
		ownKeyCache:     NewKeyCache(120*time.Second, 200*time.Second),
	}

	handler.ServeHTTP(response, request)

	status := response.Result().StatusCode
	respBody, _ := ioutil.ReadAll(response.Result().Body)
	if status != http.StatusUnauthorized {
		t.Error(
			fmt.Sprintf(
				"API Access was not granted to a non-verified user: %d - %s -- %s",
				status,
				http.StatusText(status),
				respBody,
			),
		)
	}

	if string(respBody) != "" {
		t.Error("Unauthorized response body for request with invalid token was not empty")
	}
}
